/**
 * 
 */
package gr.demokritos.iit.netcdfcsv;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ucar.ma2.Array;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

/**
 * @author Yiannis Mouchakis
 *
 * Exports CSV from NetCDF data. For each variable that is not a dimension variable a different CSV file is produced.
 *
 */
public class NetCDFToCSV {
	
	private String netcdf_path;
	private String csv_directory;
	
	/**
	 * 
	 * @param netcdf_path path of the netcdf file
	 * @param csv_directory directory to store the produced CSV files. Filenames will be {netcdf_name}_{variable_name}.csv
	 */
	public NetCDFToCSV(String netcdf_path, String csv_directory) {
		this.netcdf_path = netcdf_path;
		this.csv_directory = csv_directory;
	}
	
	/**
	 * 
	 * Exports netcdf data to CSV. If all data can be loaded on memory use export(true) instead for faster write.
	 * 	
	 * @throws IOException
	 * @throws InvalidRangeException
	 */
	public void export() throws IOException, InvalidRangeException {
		export(false);
	}
	
	/**
	 * 
	 * Exports netcdf data to CSV.
	 * 
	 * @param fastWrite true if all data can be loaded on memory for faster write, false otherwise.
	 * @throws IOException
	 * @throws InvalidRangeException
	 */
	public void export(boolean fastWrite) throws IOException, InvalidRangeException {
		
		NetcdfFile ncfile = NetcdfFile.open(netcdf_path);

		List<Variable> var_list = ncfile.getVariables();
		List<Dimension> dim_list = ncfile.getDimensions();
		
		writeDimensionWithNoData(dim_list, ncfile);

		List<Variable> measurement_variables = new ArrayList<Variable>();

		for (Variable variable : var_list) {
			if ( ! isDimensionVar(variable, dim_list)) {
				measurement_variables.add(variable);//discover measurement variables
			} else {
				writeDimensionData(variable);//or write codelist
			}
		}
		
		
		for (Variable measurement_variable : measurement_variables) {
			
			String output_filepath = csv_directory + Paths.get(netcdf_path).getFileName() + "_" + measurement_variable.getShortName() + ".csv";
			//BufferedWriter writer = new BufferedWriter( new FileWriter(output_filepath) );
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out));
			Object fill_value = getFillValue(measurement_variable);
			
			if (fastWrite) {//load values in memory and start writing.
				Array results = measurement_variable.read();
				long row = 0L;//row number
				IndexIterator it = results.getIndexIterator();
				while(it.hasNext()) {
					Object result = it.next();
					int[] indices = it.getCurrentCounter();
					writeDimensionIndices(row, indices, writer);
					//write value or empty string if fill value
					writer.write(getValue(fill_value, result));
					writer.newLine();
					row++;
				}
				
			} else {//read values one by one and write.
				
				List<Dimension> dims = measurement_variable.getDimensions();
				int n = dims.size();
				int[] indices = new int[n];
				long[] ranges = new long[n];
				int i = 0;
				for (Dimension dim : dims) {
					ranges[i] = dim.getLength();
					i++;
				}
						
				// writes observations
				// Start off at indices = [0,...,0]
				long row = 0L;//row number
				do {
					writeDimensionIndices(row, indices, writer);
					writeMeasurementData(indices, measurement_variable, fill_value, writer);
					advanceIndices(n, indices, ranges);
					row++;
                                        flushWriter(row,10000,writer);

				}
				while( ! allMaxed(n, indices, ranges) );
					//do last one
					writeDimensionIndices(row, indices, writer);
					writeMeasurementData(indices, measurement_variable, fill_value, writer);
                                        writer.flush();
                        }
			
			writer.close();
			
		}
		
		ncfile.close();
		
	}
        

	/**
	 * 
	 * writes dimension index for each variable
	 * 
	 * @param row the row number to be written
	 * @param indices array the holds current index position
	 * @param writer output writer
	 * @throws IOException
	 */
	private void writeDimensionIndices(Long row, int[] indices, BufferedWriter writer) throws IOException {
		writer.write(row.toString() + ", ");
		for (int i=0; i<indices.length; i++) {
			writer.write(indices[i] + ", ");
		}
	}
	
	/**
	 * 
	 * @param indices array the holds current index position
	 * @param measurement_variable the variable to get data from
	 * @param dim_data_ordered List that holds all dimension data
	 * @param writer
	 * @throws IOException
	 * @throws InvalidRangeException
	 */
	private void writeMeasurementData( int[] indices, Variable measurement_variable, Object fill_value, BufferedWriter writer)
	throws IOException, InvalidRangeException
	{
		
		String sectionSpec = "";//used to read values in the grid
		for (int i = 0; i < indices.length - 1; i++) {
			sectionSpec += indices[i] + ":" + indices[i] + ", ";
		}
		sectionSpec += indices[indices.length - 1] + ":" + indices[indices.length - 1];
		
		//write measurement
		Array result = measurement_variable.read(sectionSpec);
		writer.write(getValue(measurement_variable, getValue(fill_value, result.getObject(0))));
		writer.newLine();
	}
	
	
	private void writeDimensionData(Variable var) throws IOException {
		String output_filepath = csv_directory + Paths.get(netcdf_path).getFileName() + "_" + var.getShortName() + ".csv";
		BufferedWriter writer = new BufferedWriter( new FileWriter(output_filepath) );
		IndexIterator it = var.read().getIndexIterator();
		long row_no = 0L;
		while(it.hasNext()) {
			writer.write(row_no++ + ", " + getValue(getFillValue(var), it.next().toString()));
			writer.newLine();
		}
		writer.close();
	}
	
	/**
	 * 
	 * Create CSV file for each dimension that is associated with values, just an index and write the index.
	 * 
	 * @param dim_list dataset dimensions
	 * @param ncfile input file
	 * @throws IOException
	 */
	private void writeDimensionWithNoData(List<Dimension> dim_list, NetcdfFile ncfile) throws IOException {
		for (Dimension dim : dim_list) {
			if (ncfile.findVariable(dim.getShortName()) == null) {//if dimensions is not also a variable
				String output_filepath = csv_directory + Paths.get(netcdf_path).getFileName() + "_" + dim.getShortName() + ".csv";
				BufferedWriter writer = new BufferedWriter( new FileWriter(output_filepath) );
				for (int row_no=0; row_no<dim.getLength(); row_no++) {
					writer.write(new Integer(row_no).toString());
					writer.newLine();
				}
				writer.close();
			}
		}
	}
	
	/**
	 * Advances 'indices' to the next in sequence.
	 * @param n
	 * @param indices
	 * @param ranges
	 */
    private void advanceIndices(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i]+1 == ranges[i]) {
                indices[i] = 0;
            }
            else {
                indices[i] += 1;
                break;
            }
        }

    }

    /**
     * Tests if indices are in final position.
     * @param n
     * @param indices
     * @param ranges
     * @return
     */
    private boolean allMaxed(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i] != ranges[i]-1) {
                return false;
            }
        }
        return true;

    }
    
    /**
     * Private helper checking if a variable is a dimension.
     * 
     * @param variable
     * @param dim_list
     */
	private boolean isDimensionVar(Variable variable, List<Dimension> dim_list)
	{
		for( Dimension dimension : dim_list ) {
			String dimName = dimension.getShortName();
			if( (dimName != null) && dimName.equals(variable.getShortName()) )
			{ return true; }
		}
		return false;
	}
	
	/**
	 * 
	 * Checks a value against the fill value of a variable and returns the String representation of the value.
	 * 
	 * @param fill_value netcdf fill value
	 * @param value the value to check against the fill value
	 * @return empty string if value equals the fill value, the string representation of the value otherwise
	 */
	private String getValue(Object fill_value, Object value) {
		if (value.equals(fill_value)) {
			return "";
		} else {
			return value.toString();
		}
	}
	
	
	/**
	 * 
	 * Get the netcdf fill value if any
	 * 
	 * @param variable the variable to extract the fill value
	 * @return the fill value or null if not set
	 */
	private Object getFillValue(Variable variable) {
		Object fill_value = null;
		Attribute fill_value_attr = variable.findAttribute("_FillValue");
		if (fill_value_attr != null) {
			fill_value = fill_value_attr.getValue(0);
		}
		return fill_value;
	}

        /**
	 * 
	 * Flushes the stdout writer in predefined row size
	 * 
	 * @param row the row number to be written
	 * @param BufferSize the row size at which flush happens
	 * @param writer output writer
	 * @throws IOException
	 */
	private void flushWriter(long row, int BufferSize, BufferedWriter writer) throws IOException{
            if(row % BufferSize == 0){
                writer.flush();
            }
        }
 
	/**
	 * @param args args[0] netcdf location, args[1] output directory, args[2] "fast" for in memory fast export if desirable
	 * @throws InvalidRangeException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, InvalidRangeException {
		
		NetCDFToCSV netcdfToCSV = new NetCDFToCSV(args[0], args[1]);
		if (args.length == 3 && args[2].equals("fast")) {
			System.out.println("running with in-memory configuration");
			netcdfToCSV.export(true);
		} else {
			netcdfToCSV.export(false);
		}

	}

}
